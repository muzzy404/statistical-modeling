#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget * parent = 0);
  ~MainWindow();

private slots:
  void on_btnGenNum_clicked();

  void on_btnGenDataSample_clicked();

private:
  Ui::MainWindow * ui;

  // parameters
  int aUni, bUni, nBin;
  float pBin, pGeo;
  double muPoi, qLog;

  std::vector<int> dataSample;

  void getParameters();
};

#endif // MAINWINDOW_H
