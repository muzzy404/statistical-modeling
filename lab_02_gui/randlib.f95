subroutine srand_fortran()
  implicit none
  integer :: S, clock, i
  integer, allocatable :: seed(:)

! Initialize a pseudo-random number sequence
  call random_seed(size = S)
  allocate(seed(S))
  call system_clock(count = clock)
  seed = clock + 37 * (/ (i - 1, i = 1, S) /)
  call random_seed(put = seed)
  deallocate(seed)
end subroutine

! uniform distribution in range
function irnuni(ilow, iup) result(r)
  implicit none
  integer, intent(in) :: ilow, iup
  integer :: r

  real :: u
  call random_number(u)

  r = (iup - ilow + 1)*u + ilow
end function
!=======================================
! binomial distribution
function irnbin(N, p) result(r)
! r - success
! N - total
! p - probability of success
  implicit none
  integer, intent(in) :: N
  real, intent(in)    :: p

  integer :: r
  real    :: u, current_p

  call random_number(u)

  current_p = (1.0 - p)**N
  do r = 0, (N - 1)
    u = u - current_p
    if (u <= 0.0) exit
    current_p = current_p * ((p/(1.0 - p)) * ((N - r)/(r + 1.0)))
  end do

end function
!=======================================
! geometric distribution
function irngeo_1(p) result(k)
  implicit none
  real, intent(in) :: p

  integer :: k
  real(8) :: u, current_p

  call random_number(u)

  current_p = p
  k = 1
  do
    u = u - current_p
    if (u <= 0.0) exit

    k = k + 1
    current_p = current_p * (1.0 - p)
  end do
end function

function irngeo_2(p) result(k)
  implicit none
  real, intent(in) :: p

  integer :: k
  real(8) :: u

  k = 1
  do
    call random_number(u)
    if (u <= p) exit
    k = k + 1
  end do
end function

function irngeo_3(p) result(k)
  implicit none
  real, intent(in) :: p

  integer :: k
  real(8) :: u

  call random_number(u)
  k = aint(log(u)/log(1.0 - p)) + 1
end function
!=======================================
! Poisson distribution
function irnpoi_1(mu) result(r)
  implicit none
  real(8), intent(in) :: mu

  integer :: r
  real(8) :: u, current_p

  call random_number(u)

  current_p = exp(-mu)
  r = 0
  do
    u = u - current_p
    if (u <= 0.0) exit

    r = r + 1
    current_p = current_p * mu / r
  end do
end function

function irnpoi_2(mu) result(r)
  implicit none
  real(8), intent(in) :: mu

  integer :: r
  real(8) :: u, mult

  mult = 1.0
  r = 1
  do
    call random_number(u)
    mult = mult * u
    if (mult < exp(-mu)) exit
    r = r + 1
  end do
end function
!=======================================
!
function irnlog(q) result(r)
  implicit none
  real(8), intent(in) :: q

  integer :: r
  real(8) :: u, current_p

  call random_number(u)

  r = 1
  do
    current_p = -(q**r)/(dble(r) * log(1.0 - q))
    u = u - current_p

    if (u <= 0.0) exit
    r = r + 1
  end do
end function
