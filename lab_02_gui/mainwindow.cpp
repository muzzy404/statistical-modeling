#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "randlib.h"

#include <cmath>
#include <numeric> // accumulate

#include <QStringList>
#include <QString>

MainWindow::MainWindow(QWidget * parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  // init spin box with functions names
  QStringList functionsList;
  functionsList << "Uniform (irnuni)"      // 0
                << "Binomial (irnbin)"     // 1
                << "Geometric (irngeo_1)"  // 2
                << "Geometric (irngeo_2)"  // 3
                << "Geometric (irngeo_3)"  // 4
                << "Poisson (irnpoi_1)"    // 5
                << "Poisson (irnpoi_2)"    // 6
                << "Logarithmic (irnlog)"; // 7

  ui->cmbBoxFuncNames->addItems(functionsList);

  srand_fortran_();
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::getParameters()
{
  double expVal = 0.0;
  double varVal = 0.0;

  const int distr = ui->cmbBoxFuncNames->currentIndex();
  switch (distr) {
    // !!! set e and var
    case 0:
      aUni = ui->spBoxUniA->value();
      bUni = ui->spBoxUniB->value();

      expVal = (aUni + bUni) / 2.0;
      varVal = (pow((bUni - aUni + 1.0), 2) - 1.0) / 12.0;
      break;

    case 1:
      nBin = ui->spBoxBinN->value();
      pBin = ui->spBoxBinP->value();

      expVal = nBin * pBin;
      varVal = nBin * pBin * (1.0 - pBin);
      break;

    case 2:
    case 3:
    case 4:
      pGeo = ui->spBoxGeoP->value();

      expVal = 1.0 / pGeo;
      varVal = (1.0 - pGeo) / pow(pGeo, 2);
      break;

    case 5:
    case 6:
      muPoi = ui->spBoxPoiMu->value();

      expVal = muPoi;
      varVal = muPoi;
      break;

    case 7:
      qLog = ui->spBoxLogQ->value();

      double alpha = 1.0 / log(1.0 - qLog);
      expVal = -alpha * qLog / (1.0 - qLog);
      varVal = -alpha * qLog * (1.0 + alpha*qLog)/pow((1.0 - qLog), 2);
      break;
    }

  ui->lblExpValueTheor->setText(QString::number(expVal, 'f', 3));
  ui->lblVarValueTheor->setText(QString::number(varVal, 'f', 3));
}

void MainWindow::on_btnGenNum_clicked()
{
  getParameters();

  int num = 0;
  switch (ui->cmbBoxFuncNames->currentIndex()) {
    case 0: num = irnuni_(&aUni, &bUni); break;
    case 1: num = irnbin_(&nBin, &pBin); break;
    case 2: num = irngeo_1_(&pGeo);      break;
    case 3: num = irngeo_2_(&pGeo);      break;
    case 4: num = irngeo_3_(&pGeo);      break;
    case 5: num = irnpoi_1_(&muPoi);     break;
    case 6: num = irnpoi_2_(&muPoi);     break;
    case 7: num = irnlog_(&qLog);        break;

    default:
      ui->lblNumber->setText("unknown distribution");
      return;
  }

  ui->lblNumber->setText(QString::number(num));
  ui->lblExpValue->setText("-");
  ui->lblVarValue->setText("-");
}

void MainWindow::on_btnGenDataSample_clicked()
{
  getParameters();

  const int dataSize = ui->spBoxDataSize->value();

  dataSample.clear();
  switch (ui->cmbBoxFuncNames->currentIndex()) {
    case 0: // Uniform
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irnuni_(&aUni, &bUni));
      break;

    case 1: // Binomial
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irnbin_(&nBin, &pBin));
      break;

    case 2: // Geometric 1
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irngeo_1_(&pGeo));
      break;
    case 3: // Geometric 2
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irngeo_2_(&pGeo));
      break;
    case 4: // Geometric 3
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irngeo_3_(&pGeo));
      break;

    case 5: // Poisson 1
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irnpoi_1_(&muPoi));
      break;
    case 6: // Poisson 2
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irnpoi_2_(&muPoi));
      break;

    case 7: // Logarithmic
      for(int i = 0; i < dataSize; ++i)
        dataSample.push_back(irnlog_(&qLog));
      break;
    }

  double expVal = std::accumulate(dataSample.begin(),
                                  dataSample.end(), 0.0) / dataSample.size();
  double varVal = 0.0;
  for(const int num : dataSample)
  {
    varVal += pow(num - expVal, 2.0);
  }
  varVal /= dataSample.size();

  ui->lblExpValue->setText(QString::number(expVal, 'f', 3));
  ui->lblVarValue->setText(QString::number(varVal, 'f', 3));
}
