#ifndef RANDLIB_H
#define RANDLIB_H

extern "C"
{
  extern void srand_fortran_(void);

  // uniform distribution
  extern int irnuni_(int *, int *);

  // binomial distribution
  extern int irnbin_(int *, float *);

  // geometric distribution
  extern int irngeo_1_(float *);
  extern int irngeo_2_(float *);
  extern int irngeo_3_(float *);

  // Poisson distribution
  extern int irnpoi_1_(double *);
  extern int irnpoi_2_(double *);

  extern int irnlog_(double *);
}

#endif // RANDLIB_H
