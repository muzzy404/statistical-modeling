N     <- 1400
TOTAL <- 8760.0
m     <- 4

blocks <- c(4, 2, 6, 2)
L      <- c(5, 2, 8, 2)

lambda <- c(40.0, 10.0, 80.0, 30.0)
lambda <- lambda * (10**(-6))

t <- list()
for(i in 1:m) {
  t[[i]] <- seq(from = 1, to = blocks[i], by = 1)
}

for(iter in 1:10) {
  d <- 0
  for(j in 1:N) {
    
    for(i in 1:m) {
      for(l in 1:blocks[i]) {
        t[[i]][l] <- -log(runif(1)) / lambda[i]
      }
      
      for(l in 1:L[i]) {
        l <- which.min(t[[i]])
        t[[i]][l] <- t[[i]][l] - log(runif(1)) / lambda[i]
      }
    } # end of m loop
    
    x <- logical()
    for(i in 1:m) {
      for(l in 1:blocks[i]) {
        if (t[[i]][l] > TOTAL)
          x <- append(x, TRUE)
        else
          x <- append(x, FALSE)
      }
    }
    
    f <- (x[1]&&x[2] || x[3]&&x[4]) &&
         (x[5]&&x[6])               &&
         (x[7]&&x[8] || x[9]&&x[10] || x[11]&&x[12]) &&
         (x[13] || x[14])
    if (!f)
      d <- d + 1
  }
  
  P <- 1 - d/N
  print(P)
}