
program main
  use tests
  implicit none
  integer :: S, clock, i
  integer, allocatable :: seed(:)

  ! Initialize a pseudo-random number sequence
  call random_seed(size = S)
  allocate(seed(S))
  call system_clock(count = clock)
  seed = clock + 37 * (/ (i - 1, i = 1, S) /)
  call random_seed(put = seed)

  call distributions_test()

  do i = 1, 3
    call Kolmogorov_test_exp()
  end do

  write(*,*) 'all done...'
end
