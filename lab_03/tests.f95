module tests
  contains

subroutine print_table(e, e_t, v, v_t)
  implicit none
  real(8), intent(in) :: e, v
  real(4), intent(in) :: e_t, v_t

  write(*,'(A3,A17,A17,A17)') '|', 'estimation|', 'theoretical|', 'error|'
  write(*,'(A3,A17,A17,A17)') '--', '---------------', '---------------', '---------------'
  write(*,'(A3,F16.6,A1,F16.6,A1,F16.6,A1)') 'M|', e, '|', e_t, '|', abs(e - e_t), '|'
  write(*,'(A3,F16.6,A1,F16.6,A1,F16.6,A1)') 'D|', v, '|', v_t, '|', abs(v - v_t), '|'
  write(*,*)
end subroutine

function expected_value(nums, N) result(e)
  implicit none
  integer, intent(in) :: N
  real(8), dimension(N), intent(in) :: nums

  real(8) :: e

  e = sum(nums) / dble(N)
end function

function variance(nums, N, e) result(var)
  implicit none
  integer, intent(in) :: N
  real(8), intent(in) :: e
  real(8), dimension(N), intent(in) :: nums

  real(8) :: var
  integer :: i

  var = 0.0
  do i = 1, N
    var = var + (nums(i) - e)**2
  end do
  var = var / dble(N)
end function

subroutine distributions_test()
  use continuous_randlib
  use graphics

  implicit none
  integer, parameter :: N = 100000
  integer, parameter :: RUN_TEST = 1
  integer, parameter ::  NO_TEST = 0

  real(8) :: E
  real(8), dimension(N) :: nums
  integer :: i

  real,    parameter :: UNI_A     =   0.00
  real,    parameter :: UNI_B     = 100.00
  real,    parameter :: NRM_E     =   0.00 ! 14.50
  real,    parameter :: NRM_SIGMA =   1.00 ! 2.00
  real,    parameter :: EXP_BETA  =   1.0
  integer, parameter :: CHI_K     =  10
  integer, parameter :: STU_K     =  10

  ! run tests
  integer, parameter :: RUN_UNI = NO_TEST !RUN_TEST
  integer, parameter :: RUN_NRM = NO_TEST !RUN_TEST
  integer, parameter :: RUN_EXP = NO_TEST !RUN_TEST
  integer, parameter :: RUN_CHI = NO_TEST !RUN_TEST
  integer, parameter :: RUN_STD = NO_TEST !RUN_TEST

  if (RUN_UNI == RUN_TEST) then ! Uniform distribution test
    write(*,*) "Uniform distribution"
    do i = 1, N
      nums(i) = rnuni(UNI_A, UNI_B)
    end do
    E = expected_value(nums, N)
    call print_table(E, ((UNI_A + UNI_B)/2.0), &
                     variance(nums, N, E), (((UNI_B - UNI_A)**2.0)/12.0))
    call count_graphics('__rnuni', nums, N, 12)
  end if

  if (RUN_NRM == RUN_TEST) then ! Normal distribution - test
    write(*,*) "Normal distribution - algorithm 1"
    do i = 1, (N-1), 2
      nums(i:i+1) = rnrm_1(NRM_E, NRM_SIGMA)
    end do
    E = expected_value(nums, N)
    call print_table(E, NRM_E, variance(nums, N, E), NRM_SIGMA**2)
    call count_graphics('_rnrm-1', nums, N, 64)
    ! ~~~~~~~~~~~
    write(*,*) "Normal distribution - algorithm 2"
    do i = 1, N
      nums(i) = rnrm_2(NRM_E, NRM_SIGMA)
    end do
    E = expected_value(nums, N)
    call print_table(E, NRM_E, variance(nums, N, E), NRM_SIGMA**2)
    call count_graphics('_rnrm-2', nums, N, 64)
  end if

  if (RUN_EXP == RUN_TEST) then ! Exponential distribution test
    write(*,*) "Exponential distribution"
    do i = 1, N
      nums(i) = rnexp(EXP_BETA)
    end do
    E = expected_value(nums, N)
    call print_table(E, EXP_BETA, variance(nums, N, E), EXP_BETA**2)
    call count_graphics('__rnexp', nums, N, 64)
  end if

  if (RUN_CHI == RUN_TEST) then ! Chi-squared distribution test
    write(*,*) "Chi-squared distribution"
    do i = 1, N
      nums(i) = rnchis(CHI_K)
    end do
    E = expected_value(nums, N)
    call print_table(E, real(CHI_K), variance(nums, N, E), 2.0*CHI_K)
    call count_graphics('_rnchis', nums, N, 64)
  end if

  if (RUN_STD == RUN_TEST) then
    write(*,*) "Student's t-distribution"
    do i = 1, N
      nums(i) = rnstud(STU_K)
    end do
    E = expected_value(nums, N)
    call print_table(E, 0.0, variance(nums, N, E), (STU_K/real(STU_K - 2)))
    call count_graphics('_rnstud', nums, N, 64)
  end if

end subroutine

subroutine Kolmogorov_test_exp()
  use continuous_randlib
  use graphics
  implicit none
  integer, parameter :: N     = 1000
  real,    parameter :: BETA  = 3.0

  real(8), dimension(N) :: X
  real(8) :: Dp, Dm, D, D_critical!, u
  real(8) :: tmpP, tmpM, F

  integer :: i

  do i = 1, N
    X(i) = rnexp(BETA)
    !call random_number(u)
    !X(i) = u
  end do
  call selection_sort(X, N)

  i = 1
  F = 1.0 - exp(-X(i)/BETA) !X(i)
  Dp = i/dble(N) - F
  Dm = F - (i - 1)/dble(N)

  do i = 1, N
    F = 1.0 - exp(-X(i)/BETA) !X(i)
    tmpP = i/dble(N) - F
    tmpM = F - (i - 1)/dble(N)

    if (tmpP > Dp) Dp = tmpP
    if (tmpM > Dm) Dm = tmpM
  end do
  D = max(Dp, Dm)

  D_critical = 1.36 / sqrt(real(N))

  write(*,*) 'Kolmogorov-Smirnov test:'
  write(*,'(A8,F8.6)') 'E = ', expected_value(X, N)
  write(*,'(A8,F5.3)') 'D = ', D !* sqrt(real(N))
  write(*,'(A26)') '~~~~~~~~~~~~~~~~~~~~~~'
  write(*,'(A21,F5.3)') '0.05: critical = ', D_critical
  write(*,*)

end subroutine

end module
