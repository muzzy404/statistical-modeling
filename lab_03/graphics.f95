module graphics
  contains

subroutine selection_sort(a, N)
  integer, intent (in) :: N
  real(8), dimension(N), intent(in out) :: a

  integer :: i, min_index
  real(8) :: temp

  do i = 1, N - 1
     min_index = minloc(a(i:), 1) + i - 1
     if (a(i) > a(min_index)) then
        temp = a(i)
        a(i) = a(min_index)
        a(min_index) = temp
     end if
  end do
end subroutine selection_sort

subroutine count_graphics(function_name, data_sample, N, INTERVALS)
  implicit none

  character(len = 7), intent(in) :: function_name
  integer, intent(in)   :: N
  real(8), dimension(N) :: data_sample
  integer, intent(in) :: INTERVALS

  character(len = 100) :: filename
  real(8) :: probability
  integer :: i, out_f

  integer :: j, frequency
  real(8) :: step, x

  call selection_sort(data_sample, N)

  ! --- distribution ---
  write (filename, "(A12,A7,A4)") "distribution", function_name, ".dat"
  open(newunit = out_f, file = filename, status = "replace")

  do i = 1, N
    probability = dble(i) / dble(N)
    write(out_f,('(F6.3,F6.3)')) data_sample(i), probability
  end do
  close(out_f)

  ! --- density ---
  write (filename, "(A7,A7,A4)") "density", function_name, ".dat"
  open(newunit = out_f, file = filename, status = "replace")

  x = minval(data_sample)
  step = (maxval(data_sample) - x) / dble(INTERVALS)

  i = 1
  j = 1
  frequency = 0
  do
    if (i > INTERVALS) exit
    if ((data_sample(j) >= x).and.(data_sample(j) <= (x + step))) then
        frequency = frequency + 1
        j = j + 1
    else
      probability = dble(frequency) / (step * dble(N))
      write(out_f,('(F12.6,F12.6,F12.6)')) (x + step/2.0), probability, step
      x = x + step
      i = i + 1
      frequency = 0
    end if
  end do
  close(out_f)
  write(*,*) "graphics count finished..."
  write(*,*)
end subroutine

end module
