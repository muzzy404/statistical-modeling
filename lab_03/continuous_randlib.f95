module continuous_randlib
  contains

! Uniform distribution
function rnuni(a, b) result(r)
  implicit none
  real, intent(in) :: a, b
  real(8) :: u, r

  call random_number(u)
  r = (b - a)*u + a
end function

! Normal distribution 1 - Box�Muller transform
function rnrm_1(e, sigma) result(r)
  implicit none
  real(8), parameter :: PI = 4.d0 * datan(1.d0)
  real, intent(in) :: e, sigma

  real(8), dimension(2) :: u, r

  call random_number(u(1))
  call random_number(u(2))

  r(1) = (sqrt(-2.0*log(u(2))) * cos(2.0*PI*u(1))) * sigma + e
  r(2) = (sqrt(-2.0*log(u(2))) * sin(2.0*PI*u(1))) * sigma + e
end function

! Normal distribution 2
function rnrm_2(e, sigma) result(r)
  implicit none
  real, intent(in) :: e, sigma

  integer, parameter :: M = 12
  real(8) :: u, S, r
  integer :: n

  S = 0.0
  n = 0

  do
    call random_number(u)
    S = S + u
    n = n + 1

    if (n > M) exit
  end do
  r = e + sigma*(S - 0.5*n)*(6.0/sqrt(3.0*n))
end function

! Exponential distribution
function rnexp(beta) result(r)
  implicit none
  real, intent(in) :: beta
  real(8) :: u, r

  call random_number(u)
  r = -beta*log(u)
end function

! Chi-squared distribution
function rnchis(k) result(r)
  implicit none
  integer, intent(in) :: k
  real(8) :: r
  integer :: i

  r = 0.d0
  do i = 1, k
    r = r + (rnrm_2(0.0, 1.0))**2.d0
  end do
end function

! Student's t-distribution
function rnstud(k) result(r)
  implicit none
  integer, intent(in) :: k
  real(8) :: r

  r = rnrm_2(0.0, 1.0) / sqrt(rnchis(k)/dble(k))
end function

end module
