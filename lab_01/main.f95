!PRNG - Pseudorandom number generator

subroutine selection_sort(a, N)
  integer, intent (in) :: N
  real(8), dimension(N), intent(in out) :: a

  integer :: i, min_index
  real(8) :: temp

  do i = 1, N - 1
     min_index = minloc(a(i:), 1) + i - 1
     if (a(i) > a(min_index)) then
        temp = a(i)
        a(i) = a(min_index)
        a(min_index) = temp
     end if
  end do
end subroutine selection_sort

subroutine PRNG_test(N, M, D)
  implicit none

  integer, parameter :: INTERVALS = 10

  integer, intent(in)  :: N
  real(8), intent(out) :: M, D

  real(8), dimension(N) :: nums

  real(8) :: K, SUM_1, SUM_2
  real(8) :: sum_nums
  real(8) :: total
  real(8) :: step, x_1, x_2

  integer, dimension(INTERVALS) :: frequency
  real(8), dimension(N)         :: probability

  integer :: i, f, out_f

  character(len = 100) :: filename

  sum_nums = 0.0

  call random_number(nums)

  do i = 1, N
    sum_nums = sum_nums + nums(i)
  end do

  total = N

  M = sum_nums / total
  D = 0.0
  do i = 1, N
    D = D + (nums(i) - M)**2
  end do
  D = D / N

  ! Correlation function
  write (filename, "(A12,I6,A4)") "correlation ", N, ".dat"
  open(newunit = out_f, file = filename, status = "replace")
  do f = 1, N
    SUM_1 = 0.0
    SUM_2 = 0.0

    do i = 1, (N - f)
      SUM_1 = SUM_1 + (nums(i) - M)*(nums(i+f) - M)
      SUM_2 = SUM_2 + (nums(i) - M)**2
    end do
    do i = (N - f + 1), N ! rest of sum
      SUM_2 = SUM_2 + (nums(i) - M)**2
    end do

    K = SUM_1 / SUM_2
    write(out_f, '(I10,F12.6)') f, K
  end do
  close(out_f)
  ! ---------------------------------------------------------------


  ! Empirical distribution function - ok variant
  call selection_sort(nums, N)

  write (filename, "(A22,I6,A4)") "distribution function ", N, ".dat"
  open(newunit = out_f, file = filename, status = "replace")

  do f = 1, N

    probability(f) = f / total

    write(out_f,('(F12.6,F6.3)')) nums(f), probability(f)
    if (f < N) then
      write(out_f,('(F12.6,F6.3)')) nums(f + 1), probability(f)
    end if
  end do
  close(out_f)
  ! ---------------------------------------------------------------


  ! Empirical probability density function
  step = 1.0 / INTERVALS

  write (filename, "(A20,I6,A4)") "probability density ", N, ".dat"
  open(newunit = out_f, file = filename, status = "replace")

  x_1 = 0.0
  do f = 1, (INTERVALS)
    frequency(f) = 0

    x_2 = x_1 + step

    do i = 1, N
      if ((nums(i) >= x_1).and.(nums(i) < x_2)) then
        frequency(f) = frequency(f) + 1
      end if
    end do

    probability(f) = frequency(f) / (total * step)

    write(out_f,('(F12.6,F6.3)')) x_1, 0.0
    write(out_f,('(F12.6,F6.3)')) x_1, probability(f)
    write(out_f,('(F12.6,F6.3)')) x_2, probability(f)
    write(out_f,('(F12.6,F6.3)')) x_2, 0.0

    x_1 = x_2
  end do
  close(out_f)
end subroutine

subroutine print_table_row(n, m_theor, d_theor, m, d)
  implicit none

  integer, intent(in) :: n
  real(8), intent(in) :: m_theor, d_theor, m, d

  real(8) :: m_error, d_error
  m_error = abs(m_theor - m)
  d_error = abs(d_theor - d)

  write(*,'(I8,A5,F16.5,F16.5,F16.5)')  N, ' (M) ', m, m_theor, m_error
  write(*,'(A13,F16.5,F16.5,F16.5)') '(D) ', d, d_theor, d_error
  write(*,*)
end subroutine

subroutine main_task()
  implicit none

  integer :: N

  real(8) :: M, D
  real(8) :: theoretical_m, theoretical_d, a, b

  a = 0.0
  b = 1.0

  theoretical_m = (a + b) / 2
  theoretical_d = (b - a)**2 / 12

  write(*,'(A13,A16,A16,A16)') 'N (_) ', 'estimation', 'theoretical', 'error'
  write(*,*)

  N = 10
  call PRNG_test(N, M, D)
  call print_table_row(N, theoretical_m, theoretical_d, M, D)

  N = 100
  call PRNG_test(N, M, D)
  call print_table_row(N, theoretical_m, theoretical_d, M, D)

  N = 1000
  call PRNG_test(N, M, D)
  call print_table_row(N, theoretical_m, theoretical_d, M, D)

  N = 10000
  call PRNG_test(N, M, D)
  call print_table_row(N, theoretical_m, theoretical_d, M, D)

  N = 50000
  call PRNG_test(N, M, D)
  call print_table_row(N, theoretical_m, theoretical_d, M, D)
end subroutine

function sequences_test(seq, N, m) result (chi_squared)
  implicit none

  integer, intent(in) :: N, m
  integer, dimension(N), intent(in) :: seq

  integer, dimension(2**m) :: frequency
  integer :: i, j, min_index, lenth
  real(8) :: chi_squared, part

  frequency = 0
  do i = 1, ((N/m) * m), m

    min_index = 1
    lenth = size(frequency)

    do j = i, (i + m - 1)
      lenth = lenth / 2
      if (seq(j) == 1) then
        min_index = min_index + lenth
      end if
    end do

    frequency(min_index) = frequency(min_index) + 1
  end do

  part = size(frequency)
  part = 1/part
  part = part * N / m
  part = nint(part)

  chi_squared = 0.0
  do i = 1, size(frequency)
    chi_squared = chi_squared + (frequency(i) - part)**2
  end do
  chi_squared = chi_squared / part

  !chi_squared = nint(chi_squared)
end function

subroutine extra_task()
  ! default test
  integer, dimension(50) :: nums_1

  ! random test
  integer, dimension(50) :: nums_2

  real(8) :: sequences_test
  real(8) :: res, num
  integer :: i

  nums_1 = (/1,1,0, 0,1,0, 0,1,0, 0,0,0, 1,1,1, 1,1,1, 0,1,1, 0,1,0, &
             1,0,1, 0,0,0, 1,0,0, 0,1,0, 0,0,0, 1,0,1, 1,0,1, 0,0,0, 1,1/)

  do i = 1, size(nums_2)
    call random_number(num)
    nums_2(i) = nint(num)
  end do

  do i = 2, 4
    res = sequences_test(nums_1, size(nums_1), i)
    write(*,*) i, ':', nint(res), res
  end do

  write(*,*) '-------------------------------------------------'

  do i = 2, 4
    res = sequences_test(nums_2, size(nums_2), i)
    write(*,*) i, ':', nint(res), res
  end do
end subroutine

program main
  implicit none

  integer :: S, clock, i
  integer, allocatable :: seed(:)

  ! Initialize a pseudo-random number sequence
  call random_seed(size = S)
  allocate(seed(S))
  call system_clock(count = clock)
  seed = clock + 37 * (/ (i - 1, i = 1, S) /)
  call random_seed(put = seed)

  !call main_task()
  call extra_task()

  deallocate(seed)
end
