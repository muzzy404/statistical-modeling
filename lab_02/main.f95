! LAB 2

program main
  use RANDLIB_TESTS

  implicit none
  integer :: S, clock, i
  integer, allocatable :: seed(:)

! Initialize a pseudo-random number sequence
  call random_seed(size = S)
  allocate(seed(S))
  call system_clock(count = clock)
  seed = clock + 37 * (/ (i - 1, i = 1, S) /)
  call random_seed(put = seed)

  write(*,*) 'irnuni test, 1 -- 100, 100 000'
  call test_irnuni(100000, 1, 100)

  write(*,*) 'irnbin test, N = 10, p = 0.5, 100 000'
  call test_irnbin(100000, 10, 0.5)
  !call test_bin(100000, 30, 0.2)

  write(*,*) 'irngeo test, p = 0.5, 100 000'
  call test_irngeo(100000, 0.5)
  !call test_irngeo(100000, 0.95)

  write(*,*) 'irnpoi test, mu = 10.0, 100 000'
  call test_irnpoi(100000, dble(10.0))

  write(*,*) 'irnlog test, q = 0.5, 100 000'
  call test_irnlog(100000, dble(0.5))

  call graphics_test()

  call extra_task()

  deallocate(seed)
end
