module RANDLIB_TESTS
  contains

subroutine print_table(m, m_t, d, d_t)
  implicit none
  real(8), intent(in) :: m, m_t, d, d_t

  write(*,'(A3,A17,A17,A17)') '|', 'estimation|', 'theoretical|', 'error|'
  write(*,'(A3,A17,A17,A17)') '--', '---------------', '---------------', '---------------'
  write(*,'(A3,F16.6,A1,F16.6,A1,F16.6)') 'M|', m, '|', m_t, '|', abs(m - m_t)
  write(*,'(A3,F16.6,A1,F16.6,A1,F16.6)') 'D|', d, '|', d_t, '|', abs(d - d_t)
  write(*,*)
end subroutine

subroutine print_table_3(m, d, m_1, m_2, m_3, d_1, d_2, d_3)
  implicit none
  real(8), intent(in) :: m, m_1, m_2, m_3
  real(8), intent(in) :: d, d_1, d_2, d_3

  write(*,'(A3,A15,A15,A15,A15)') '|', 'algorithm 1|', 'algorithm 2|', 'algorithm 3|', 'theoretical|'
  write(*,'(A3,A15,A15,A15,A15)') '--', '-------------', '-------------', '-------------', '-------------'
  write(*,'(A3,F14.6,A1,F14.6,A1,F14.6,A1,F14.6)') 'M|', m_1, '|', m_2, '|', m_3, '|', m
  write(*,'(A3,F14.6,A1,F14.6,A1,F14.6,A1,F14.6)') 'D|', d_1, '|', d_2, '|', d_3, '|', d
  write(*,*)
end subroutine

function expected_value(nums, N) result(e)
  implicit none
  integer, intent(in) :: N
  integer, dimension(N), intent(in) :: nums

  real(8) :: e, total
  total = N

  e = sum(nums) / total
end function

function dispersion(nums, N, m) result(d)
  implicit none
  integer, intent(in) :: N
  real(8), intent(in) :: m
  integer, dimension(N), intent(in) :: nums

  real(8) :: d
  integer :: i

  d = 0.0
  do i = 1, N
    d = d + (nums(i) - m)**2
  end do
  d = d / N
end function
! ---------------------------------------------------

! ===== TESTS =====
subroutine test_irnuni(N, ilow, iup)
  use RANDLIB
  implicit none
  integer, intent(in) :: N, ilow, iup

  real(8) :: m, d, m_theor, d_theor
  integer :: i

  integer, dimension(N) :: nums

  do i = 1, N
    nums(i) = irnuni(ilow, iup)
  end do

  m = expected_value(nums, N)
  d = dispersion(nums, N, m)

  m_theor = (ilow + iup)/2.0
  d_theor = ((iup - ilow + 1)**2 - 1)/12.0

  call print_table(m, m_theor, d, d_theor)
end subroutine

subroutine test_irnbin(M, N, p)
  use RANDLIB
  implicit none
  integer, intent(in) :: M, N
  real, intent(in) :: p

  real(8) :: e, d, e_theor, d_theor

  integer :: i

  integer, dimension(M) :: nums

  do i = 1, M
    nums(i) = irnbin(N, p)
  end do

  e = expected_value(nums, M)
  d = dispersion(nums, M, e)

  e_theor = N * p
  d_theor = N * p * (1.0 - p)

  call print_table(e, e_theor, d, d_theor)
end subroutine

subroutine test_irngeo(N, p)
  use RANDLIB
  implicit none
  integer, intent(in) :: N
  real, intent(in) :: p

  real(8) :: e_theor, d_theor
  real(8) :: e_1, e_2, e_3
  real(8) :: d_1, d_2, d_3
  integer :: i

  integer, dimension(N) :: nums

  do i = 1, N
    nums(i) = irngeo_1(p)
  end do
  e_1 = expected_value(nums, N)
  d_1 = dispersion(nums, N, e_1)

  do i = 1, N
    nums(i) = irngeo_2(p)
  end do
  e_2 = expected_value(nums, N)
  d_2 = dispersion(nums, N, e_2)

  do i = 1, N
    nums(i) = irngeo_3(p)
  end do
  e_3 = expected_value(nums, N)
  d_3 = dispersion(nums, N, e_3)

  e_theor = 1.0/p
  d_theor = (1.0 - p)/(p**2)

  ! change to 1 !!!
  call print_table_3(e_theor, d_theor, e_1, e_2, e_3, d_1, d_2, d_3)
end subroutine

subroutine test_irnpoi(N, mu)
  use RANDLIB
  implicit none
  integer, intent(in) :: N
  real(8), intent(in) :: mu

  real(8) :: e, d
  integer :: i

  integer, dimension(N) :: nums

  do i = 1, N
    nums(i) = irnpoi_1(mu)
  end do
  e = expected_value(nums, N)
  d = dispersion(nums, N, e)
  write(*,*) 'algorithm 1'
  call print_table(e, mu, d, mu)

  do i = 1, N
    nums(i) = irnpoi_2(mu)
  end do
  e = expected_value(nums, N)
  d = dispersion(nums, N, e)
  write(*,*) 'algorithm 2'
  call print_table(e, mu, d, mu)
end subroutine

subroutine test_irnlog(N, q)
  use RANDLIB
  implicit none
  integer, intent(in) :: N
  real(8), intent(in) :: q

  real(8) :: e, d, e_t, d_t, alpha
  integer :: i

  integer, dimension(N) :: nums

  do i = 1, N
    nums(i) = irnlog(q)
  end do

  e = expected_value(nums, N)
  d = dispersion(nums, N, e)

  alpha = 1.0 / log(1.0 - q)
  e_t = -alpha * q / (1.0 - q)
  d_t = -alpha * q * (1.0 + alpha*q)/((1.0 - q)**2)

  call print_table(e, e_t, d, d_t)
end subroutine

! ===== GRAPHS =====
subroutine selection_sort(a, N)
  integer, intent (in) :: N
  integer, dimension(N), intent(in out) :: a

  integer :: i, min_index
  integer :: temp

  do i = 1, N - 1
     min_index = minloc(a(i:), 1) + i - 1
     if (a(i) > a(min_index)) then
        temp = a(i)
        a(i) = a(min_index)
        a(min_index) = temp
     end if
  end do
end subroutine selection_sort

subroutine count_graphics(function_name, data_sample, N)
  implicit none

  character(len = 8), intent(in) :: function_name
  integer, intent(in)   :: N
  integer, dimension(N) :: data_sample

  character(len = 100) :: filename
  real(8) :: probability
  integer :: i, out_f

  integer, parameter :: INTERVALS = 10
  integer :: j, frequency
  real(8) :: step, x

  call selection_sort(data_sample, N)

  ! --- distribution ---

  write (filename, "(A13,A8,A4)") "distribution ", function_name, ".dat"
  open(newunit = out_f, file = filename, status = "replace")

  do i = 1, N
    probability = dble(i) / dble(N)
    write(out_f,('(I10.6,F6.3)')) data_sample(i), probability
  end do

  close(out_f)

  ! --- density ---
  write (filename, "(A8,A8,A4)") "density ", function_name, ".dat"
  open(newunit = out_f, file = filename, status = "replace")

  x = dble(minval(data_sample))
  step = (maxval(data_sample) - x) / dble(INTERVALS)
  !write(*,*) 'step = ', step

  do i = 1, INTERVALS
    frequency = 0

    do j = 1, N
      if ((data_sample(j) >= x).and.(data_sample(j) < (x + step))) then
        frequency = frequency + 1
      end if
    end do

    probability = dble(frequency) / (step * dble(N))
    write(out_f,('(F12.6,F12.6,F12.6)')) (x + step/2.0), probability, step
    x = x + step
  end do

  !write(*,*) 'all = ', check_all
  close(out_f)
end subroutine

! ===== GRAPHICS TEST =====
subroutine graphics_test()
  use RANDLIB
  implicit none

  integer, parameter :: N = 10000
  integer, dimension (N) :: data_sample_1, &
                            data_sample_2, &
                            data_sample_3
  integer :: i

  do i = 1, N
    data_sample_1(i) = irnuni(1, 100)     ! uniform
    data_sample_2(i) = irnbin(40, 0.5)    ! irnbin(10, 0.5)   ! binomial
    data_sample_3(i) = irnlog(dble(0.66)) ! irnlog(dble(0.5)) ! log
  end do
  call count_graphics("  irnuni", data_sample_1, N)
  call count_graphics("  irnbin", data_sample_2, N)
  call count_graphics("  irnlog", data_sample_3, N)

  ! geometric
  do i = 1, N
    data_sample_1(i) = irngeo_1(0.5)
    data_sample_2(i) = irngeo_2(0.5)
    data_sample_3(i) = irngeo_3(0.5)
  end do
  call count_graphics("irngeo-1", data_sample_1, N)
  call count_graphics("irngeo-2", data_sample_2, N)
  call count_graphics("irngeo-3", data_sample_3, N)

  ! Poisson
  do i = 1, N
    data_sample_1(i) = irnpoi_1(dble(10.0))
    data_sample_2(i) = irnpoi_2(dble(10.0))
  end do
  call count_graphics("irnpoi-1", data_sample_1, N)
  call count_graphics("irnpoi-2", data_sample_2, N)
end subroutine

function factorial(k) result(r)
  implicit none
  integer, intent(in) :: k

  integer :: i
  real(8) :: r
  r = 1.d0
  do i = 2, k
    r = r * i
  end do
end function

subroutine extra_task()
  use RANDLIB
  implicit none
  real(8), parameter :: MU = 10.d0
  integer, parameter :: N = 100000

  integer :: i, out_f

  open(newunit = out_f, file = "extra-task.dat", status = "replace")
  do i = 1, N
    write(out_f,*) irnpoi_1(MU)
  end do
  close(out_f)

  write(*,*) "all done..."
end subroutine


end module RANDLIB_TESTS
